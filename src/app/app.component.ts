import { HttpClient } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  conversion: any;
  persons: any[] = [];

  person: any;
  nombre: string;
  primer_ap: string;
  segundo_ap:string;
  telefono: string;

  constructor(private http:HttpClient) { }

  ngOnInit() {
    this.http.get("http://localhost:8080/persona")
    .subscribe(data => {
      this.conversion=data;
      this.persons=this.conversion
    })
  }

  verDatos(id){
    this.http.get("http://localhost:8080/persona/"+id)
    .subscribe(data => {
      this.person = data
      this.nombre = this.person.nombre;
      this.primer_ap = this.person.primer_apellido;
      this.segundo_ap = this.person.segundo_apellido;
      this.telefono = this.person.telefono;
    })
  } 
}